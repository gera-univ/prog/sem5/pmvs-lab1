qemu-system-x86_64 \
	-drive file=ubuntuserver.qcow,format=qcow2 \
	-enable-kvm -m 4G -smp 4 \
	-device virtio-net,netdev=vmnic -netdev user,id=vmnic,hostfwd=tcp:127.0.0.1:2222-:22 \
	-device virtio-net,netdev=hn1,mac=e6:c8:ff:09:76:99 -netdev bridge,id=hn1
