# Задание 6

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 15:47:20+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="173" LINES="50"]
yanush13@vm1:~$ sudo addgroup student13u
[sudo] password for yanush13:
Adding group `student13u' (GID 1001) ...
Done.
yanush13@vm1:~$ sudo usermod -a -G student13u yanush13
yanush13@vm1:~$ sudo chgrp student13 . -R
chgrp: invalid group: ‘student13’
yanush13@vm1:~$ sudo chgrp student13u . -R
yanush13@vm1:~$ ls -la
total 128
drwxr-x--- 5 yanush13 student13u  4096 Sep  5 15:47 .
drwxr-xr-x 3 root     root        4096 Sep  3 09:04 ..
-rw------- 1 yanush13 student13u  3239 Sep  5 15:47 .bash_history
-rw-r--r-- 1 yanush13 student13u   220 Mar 19 16:02 .bash_logout
-rw-r--r-- 1 yanush13 student13u  3771 Mar 19 16:02 .bashrc
drwx------ 2 yanush13 student13u  4096 Sep  3 09:04 .cache
-rw------- 1 yanush13 student13u    54 Sep  5 09:30 .lesshst
-rw-r--r-- 1 yanush13 student13u   807 Mar 19 16:02 .profile
-rwxrwxr-x 1 yanush13 student13u    57 Sep  5 14:39 setupnet.sh
drwx------ 2 yanush13 student13u  4096 Sep  5 09:48 .ssh
-rw-r--r-- 1 yanush13 student13u     0 Sep  3 09:38 .sudo_as_admin_successful
-rw-r--r-- 1 yanush13 student13u 10434 Sep  5 09:51 task3Yanush13r
-rw-rw-r-- 1 yanush13 student13u  4610 Sep  5 09:34 task3Yanush13u
-rw-rw-r-- 1 yanush13 student13u 16004 Sep  5 09:52 task4Yanush13u
-rw-rw-r-- 1 yanush13 student13u   372 Sep  5 15:47 task56anush13u
-rw-rw-r-- 1 yanush13 student13u 11559 Sep  5 12:08 task5Yanush1301u
-rw-rw-r-- 1 yanush13 student13u     0 Sep  5 15:47 task6anush13u
-rw-rw-r-- 1 yanush13 student13u  1350 Sep  5 09:34 timelog3Yanush13u
-rw-rw-r-- 1 yanush13 student13u  4716 Sep  5 09:52 timelog4Yanush13u
-rw-rw-r-- 1 yanush13 student13u 10429 Sep  5 12:08 timelog5Yanush1301u
-rw-rw-r-- 1 yanush13 student13u     0 Sep  5 15:47 timelog6Yanush13u
-rw------- 1 yanush13 student13u  8008 Sep  5 14:39 .viminfo
drwxrwxr-x 8 yanush13 student13u  4096 Sep  5 09:49 yanush
yanush13@vm1:~$ sudo chmod g+rw .^C
yanush13@vm1:~$ sudo chmod g+rw .
yanush13@vm1:~$ ls -la
total 128
drwxrwx--- 5 yanush13 student13u  4096 Sep  5 15:47 .
drwxr-xr-x 3 root     root        4096 Sep  3 09:04 ..
-rw------- 1 yanush13 student13u  3239 Sep  5 15:47 .bash_history
-rw-r--r-- 1 yanush13 student13u   220 Mar 19 16:02 .bash_logout
-rw-r--r-- 1 yanush13 student13u  3771 Mar 19 16:02 .bashrc
drwx------ 2 yanush13 student13u  4096 Sep  3 09:04 .cache
-rw------- 1 yanush13 student13u    54 Sep  5 09:30 .lesshst
-rw-r--r-- 1 yanush13 student13u   807 Mar 19 16:02 .profile
-rwxrwxr-x 1 yanush13 student13u    57 Sep  5 14:39 setupnet.sh
drwx------ 2 yanush13 student13u  4096 Sep  5 09:48 .ssh
-rw-r--r-- 1 yanush13 student13u     0 Sep  3 09:38 .sudo_as_admin_successful
-rw-r--r-- 1 yanush13 student13u 10434 Sep  5 09:51 task3Yanush13r
-rw-rw-r-- 1 yanush13 student13u  4610 Sep  5 09:34 task3Yanush13u
-rw-rw-r-- 1 yanush13 student13u 16004 Sep  5 09:52 task4Yanush13u
-rw-rw-r-- 1 yanush13 student13u   372 Sep  5 15:47 task56anush13u
-rw-rw-r-- 1 yanush13 student13u 11559 Sep  5 12:08 task5Yanush1301u
-rw-rw-r-- 1 yanush13 student13u     0 Sep  5 15:47 task6anush13u
-rw-rw-r-- 1 yanush13 student13u  1350 Sep  5 09:34 timelog3Yanush13u
-rw-rw-r-- 1 yanush13 student13u  4716 Sep  5 09:52 timelog4Yanush13u
-rw-rw-r-- 1 yanush13 student13u 10429 Sep  5 12:08 timelog5Yanush1301u
-rw-rw-r-- 1 yanush13 student13u     0 Sep  5 15:47 timelog6Yanush13u
-rw------- 1 yanush13 student13u  8008 Sep  5 14:39 .viminfo
drwxrwxr-x 8 yanush13 student13u  4096 Sep  5 09:49 yanush
yanush13@vm1:~$ sudo addu^C
yanush13@vm1:~$ man adduser
yanush13@vm1:~$ sudo adduser --group 1001
adduser: Please enter a username matching the regular expression configured
via the NAME_REGEX[_SYSTEM] configuration variable.  Use the `--force-badname'
option to relax this check or reconfigure NAME_REGEX.
yanush13@vm1:~$ sudo adduser --group 1001 student13u
adduser: Specify only one name in this mode.
yanush13@vm1:~$ sudo adduser student13u --group 1001
adduser: Specify only one name in this mode.
yanush13@vm1:~$ sudo adduser student13u --group student13u
adduser: Specify only one name in this mode.
yanush13@vm1:~$ man adduser
yanush13@vm1:~$ sudo addusercm^Ctudent13u
yanush13@vm1:~$ man useradd
yanush13@vm1:~$ sudo useradd -g student13u student13u
yanush13@vm1:~$ sudo su student13u
$ touch file
$ rm file
$ exit
yanush13@vm1:~$ sudo chmod g-^C
yanush13@vm1:~$ sudo chown yanush13:yanush13 . -R
yanush13@vm1:~$ ls -a
.              .bash_logout  .lesshst     .ssh                       task3Yanus
h13u  task5Yanush1301u   timelog4Yanush13u    .viminfo
..             .bashrc       .profile     .sudo_as_admin_successful  task4Yanus
h13u  task6anush13u      timelog5Yanush1301u  yanush
.bash_history  .cache        setupnet.sh  task3Yanush13r             task56anus
h13u  timelog3Yanush13u  timelog6Yanush13u
yanush13@vm1:~$ ls -la
total 172
drwxrwx--- 5 yanush13 yanush13  4096 Sep  5 15:51 .
-rw-r--r-- 1 yanush13 yanush13     0 Sep  3 09:38 .sudo_as_admin_successful
-rw-r--r-- 1 yanush13 yanush13 10434 Sep  5 09:51 task3Yanush13r
-rw-rw-r-- 1 yanush13 yanush13  4610 Sep  5 09:34 task3Yanush13u
-rw-rw-r-- 1 yanush13 yanush13 16004 Sep  5 09:52 task4Yanush13u
-rw-rw-r-- 1 yanush13 yanush13   372 Sep  5 15:47 task56anush13u
-rw-rw-r-- 1 yanush13 yanush13 11559 Sep  5 12:08 task5Yanush1301u
-rw-rw-r-- 1 yanush13 yanush13 36864 Sep  5 15:51 task6anush13u
-rw-rw-r-- 1 yanush13 yanush13  1350 Sep  5 09:34 timelog3Yanush13u
-rw-rw-r-- 1 yanush13 yanush13  4716 Sep  5 09:52 timelog4Yanush13u
-rw-rw-r-- 1 yanush13 yanush13 10429 Sep  5 12:08 timelog5Yanush1301u
-rw-rw-r-- 1 yanush13 yanush13  8192 Sep  5 15:51 timelog6Yanush13u
-rw------- 1 yanush13 yanush13  8008 Sep  5 14:39 .viminfo
drwxrwxr-x 8 yanush13 yanush13  4096 Sep  5 09:49 yanush
yanush13@vm1:~$ exit
exit

Script done on 2021-09-05 15:52:41+00:00 [COMMAND_EXIT_CODE="0"]
```

## Журнал Raspbian:

```
Script started on 2021-09-05 18:54:08+03:00 [TERM="st-256color" TTY="/dev/pts/1
" COLUMNS="173" LINES="50"]
pi@vm2:~ $ sudo groupadd student13r
pi@vm2:~ $ sudo useradd student13r -g student13r
pi@vm2:~ $ sudo su student1^C
pi@vm2:~ $ ls
2021-09-03-205604_800x600_scrot.png  Desktop    herman@10.0.0.1  Pictures  task
3Yanush13r    Templates   yanush
2021-09-03-225710_800x600_scrot.png  Documents  MagPi            Public    task
5Yanush1301r  typescript
2021-09-04-191821_800x600_scrot.png  Downloads  Music            scr.png   task
6Yanush13r    Videos
pi@vm2:~ $ sudo chown pi:student13r Downloads/
pi@vm2:~ $ sudo su stude^C
pi@vm2:~ $ ls -la
total 820
drwxr-xr-x 17 pi   pi           4096 Sep  5 18:54 .
...
pi@vm2:~ $ sudo chmod g+w Downloads/
pi@vm2:~ $ sudo su student13r
student13r@vm2:/home/pi$ cd Dow
bash: cd: Dow: No such file or directory
student13r@vm2:/home/pi$ cd Downloads/
student13r@vm2:/home/pi/Downloads$ ls
student13r@vm2:/home/pi/Downloads$ mkdir s
student13r@vm2:/home/pi/Downloads$ rmdir s
student13r@vm2:/home/pi/Downloads$ exit
exit
pi@vm2:~ $ sudo chown pi:pi downloads
chown: cannot access 'downloads': No such file or directory
pi@vm2:~ $ sudo chown pi:pi Downloads/
pi@vm2:~ $ sudo chmod g-w Downloads/
pi@vm2:~ $ exit
exit

Script done on 2021-09-05 18:55:54+03:00 [COMMAND_EXIT_CODE="0"]
```
