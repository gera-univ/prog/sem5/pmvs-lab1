# Задание 9

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 16:38:36+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="77" LINES="50"]
yanush13@vm1:~$ rsync -a rsync-backup/ pi@10.0.0.2:rsync-backup
The authenticity of host '10.0.0.2 (10.0.0.2)' can't be established.
ECDSA key fingerprint is SHA256:e9uwL6bve4EGi+OJ9Aps8NMFrCyl91CaZej+zKRLL+U.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.0.0.2' (ECDSA) to the list of known hosts.
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.2's password:
Permission denied, please try again.
pi@10.0.0.2's password:
Permission denied, please try again.
pi@10.0.0.2's password:
^C^[[Arsync error: received SIGINT, SIGTERM, or SIGHUP (code 20) at rsync.c(701
) [sender=3.2.3]
yanush13@vm1:~$ rsync -a rsync-backup/ pi@10.0.0.3:rsync-backup
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
yanush13@vm1:~$ rm -rf rsync-backup/
yanush13@vm1:~$ ls rsync-backup/
task3Yanush13r  task5Yanush1301r  task6Yanush13u  task9Yanush13r
task3Yanush13u  task5Yanush1301u  task7Yanush13u  task9Yanush13u
task4Yanush13u  task6Yanush13r    task8Yanush13u
yanush13@vm1:~$ exit
exit

Script done on 2021-09-05 16:40:04+00:00 [COMMAND_EXIT_CODE="0"]
```

## Журнал Raspbian:

```
Script started on 2021-09-05 19:38:12+03:00 [TERM="st-256color" TTY="/dev/pts/1
" COLUMNS="95" LINES="50"]
pi@vm2:~ $ rm -r backups/
pi@vm2:~ $ rm -r backups/^C
pi@vm2:~ $ ls rsync-backup/
task3Yanush13r  task5Yanush1301r  task6Yanush13u  task9Yanush13r
task3Yanush13u  task5Yanush1301u  task7Yanush13u  task9Yanush13u
task4Yanush13u  task6Yanush13r    task8Yanush13u
pi@vm2:~ $ rsync -a rsync-backup/ yanush13@10.0.0.2:rsync-backup
yanush13@10.0.0.2's password:
pi@vm2:~ $ exit
exit

Script done on 2021-09-05 19:40:02+03:00 [COMMAND_EXIT_CODE="0"]

```
