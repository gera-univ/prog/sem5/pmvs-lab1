# Задание 8

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 16:08:51+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="173" LINES="50"]
yanush13@vm1:~$ man gzip
yanush13@vm1:~$ gzip -c
.bash_history              .lesshst                   .ssh/
  task56anush13u             timelog3Yanush13u          timelog8Yanush13u
.bash_logout               .local/                    .sudo_as_admin_successful
  task5Yanush1301u           timelog4Yanush13u          .viminfo
.bashrc                    .profile                   task3Yanush13r
  task6anush13u              timelog5Yanush1301u        yanush/
.cache/                    ps.log                     task3Yanush13u
  task7Yanush13u             timelog6Yanush13u          yanush-lsof.log
.config/                   setupnet.sh                task4Yanush13u
  task8Yanush13u             timelog7Yanush13u          yanush-vmstat.log
yanush13@vm1:~$ gzip -c
.bash_history              .lesshst                   .ssh/
  task56anush13u             timelog3Yanush13u          timelog8Yanush13u
.bash_logout               .local/                    .sudo_as_admin_successful
  task5Yanush1301u           timelog4Yanush13u          .viminfo
.bashrc                    .profile                   task3Yanush13r
  task6anush13u              timelog5Yanush1301u        yanush/
.cache/                    ps.log                     task3Yanush13u
  task7Yanush13u             timelog6Yanush13u          yanush-lsof.log
.config/                   setupnet.sh                task4Yanush13u
  task8Yanush13u             timelog7Yanush13u          yanush-vmstat.log
yanush13@vm1:~$ gzip -c ps.log >ps.log.tar.gz
yanush13@vm1:~$ gzip -t ps.log.tar.gz
yanush13@vm1:~$ scp ps.log.tar.gz pi@10.0.0.3:
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
ps.log.tar.gz
                                                              0%    0     0.0KB
ps.log.tar.gz
                                                            100% 1178   269.3KB
/s   00:00
yanush13@vm1:~$ ssh pi@10.0.0.3
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
Linux vm2 5.4.51 #1 Sat Aug 8 23:28:32 +03 2020 armv6l

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Sun Sep  5 18:53:52 2021 from 10.0.0.2
pi@vm2:~ $ gzip -d
.cache/        Desktop/       Downloads/     .local/        Music/         ps.l
og.tar.gz  .ssh/          Videos/
.config/       Documents/     .gnupg/        MagPi/         Pictures/      Publ
ic/        Templates/     yanush/
pi@vm2:~ $ gzip -d ps.log.tar.gz
pi@vm2:~ $ tail ps.log.tar
    957 ?        00:00:00 (sd-pam)
    962 tty1     00:00:00 bash
   1124 ?        00:00:00 kworker/u8:3-events_unbound
   1128 ?        00:00:00 kworker/u8:2-events_power_efficient
   1129 ?        00:00:00 sshd
   1200 ?        00:00:00 sshd
   1201 pts/0    00:00:00 bash
   1416 pts/0    00:00:00 script
   1417 pts/1    00:00:00 bash
   1423 pts/1    00:00:00 ps
pi@vm2:~ $ tail ps.log.tar ^C
pi@vm2:~ $ # .tar было лишним
pi@vm2:~ $ exit
logout
Connection to 10.0.0.3 closed.
yanush13@vm1:~$ tar . -f home.tar
tar: invalid option -- '.'
Try 'tar --help' or 'tar --usage' for more information.
yanush13@vm1:~$ tar - -f home.tar
-A  -c  -d  -r  -t  -u  -x
yanush13@vm1:~$ tar - -f home.tar
-A  -c  -d  -r  -t  -u  -x
yanush13@vm1:~$ tar - -f home.tar
-A  -c  -d  -r  -t  -u  -x
yanush13@vm1:~$ tar -f home.tar .
tar: You must specify one of the '-Acdtrux', '--delete' or '--test-label' optio
ns
Try 'tar --help' or 'tar --usage' for more information.
yanush13@vm1:~$ tar -f home.tar /hom^C
yanush13@vm1:~$ ls
ps.log         task3Yanush13r  task56anush13u    task7Yanush13u     timelog4Yan
ush13u    timelog7Yanush13u  yanush-lsof.log
ps.log.tar.gz  task3Yanush13u  task5Yanush1301u  task8Yanush13u     timelog5Yan
ush1301u  timelog8Yanush13u  yanush-vmstat.log
setupnet.sh    task4Yanush13u  task6anush13u     timelog3Yanush13u  timelog6Yan
ush13u    yanush
yanush13@vm1:~$ yar * -f home.tar
Command 'yar' not found, but there are 21 similar ones.
yanush13@vm1:~$ tar * -f home.tar
tar: invalid option -- '.'
Try 'tar --help' or 'tar --usage' for more information.
yanush13@vm1:~$ tl^C * -f home.tar
yanush13@vm1:~$ ls
ps.log         task3Yanush13r  task56anush13u    task7Yanush13u     timelog4Yan
ush13u    timelog7Yanush13u  yanush-lsof.log
ps.log.tar.gz  task3Yanush13u  task5Yanush1301u  task8Yanush13u     timelog5Yan
ush1301u  timelog8Yanush13u  yanush-vmstat.log
setupnet.sh    task4Yanush13u  task6anush13u     timelog3Yanush13u  timelog6Yan
ush13u    yanush
yanush13@vm1:~$ tar yanush -f yanush.tar
tar: invalid option -- 'y'
Try 'tar --help' or 'tar --usage' for more information.
yanush13@vm1:~$ tar yanush -rf yanush.tar
tar: invalid option -- 'y'
Try 'tar --help' or 'tar --usage' for more information.
yanush13@vm1:~$
yanush13@vm1:~$ tar yanush/ -rf yanush.tar
tar: invalid option -- 'y'
Try 'tar --help' or 'tar --usage' for more information.
yanush13@vm1:~$ tar -r yanush/ -f yanush.tar
yanush13@vm1:~$ gzip yanush.tar
yanush13@vm1:~$ scp yanush.tar.gz pi@10.0.0.3:
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
yanush.tar.gz
                                                              0%    0     0.0KB
yanush.tar.gz
                                                            100% 3017KB   3.9MB
/s   00:00
yanush13@vm1:~$ ssh pi@10.0.0.3
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
Linux vm2 5.4.51 #1 Sat Aug 8 23:28:32 +03 2020 armv6l

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Sun Sep  5 19:09:52 2021 from 10.0.0.2
gzpi@vm2:~ $ gzip -d yanush.tar.gz
pi@vm2:~ $ man tar
pi@vm2:~ $ tar -x yanush.tar
tar: Refusing to read archive contents from terminal (missing -f option?)
tar: Error is not recoverable: exiting now
pi@vm2:~ $ tar -x fyanush.tar
tar: Refusing to read archive contents from terminal (missing -f option?)
tar: Error is not recoverable: exiting now
pi@vm2:~ $ tar -x -f yanush.tar
pi@vm2:~ $ ls
2021-09-03-205604_800x600_scrot.png  Desktop    herman@10.0.0.1  Pictures    sc
r.png           task6Yanush13r  Videos
2021-09-03-225710_800x600_scrot.png  Documents  MagPi            ps.log.tar  ta
sk3Yanush13r    Templates       yanush
2021-09-04-191821_800x600_scrot.png  Downloads  Music            Public      ta
sk5Yanush1301r  typescript      yanush.tar
pi@vm2:~ $ rm -rf yanush*
pi@vm2:~ $ exit
logout
Connection to 10.0.0.3 closed.
yanush13@vm1:~$ exit
exit

Script done on 2021-09-05 16:14:00+00:00 [COMMAND_EXIT_CODE="0"]
```
