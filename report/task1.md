# Задание 1

## Создание виртуальной машины с ubuntu server

Создание диска в qemu

```
$ qemu-img create -f qcow2 ubuntuserver.qcow 5G
```

Запуск виртуальной машины с подмонтированным установочным образом

```
$ qemu-system-x86_64 -drive file=ubuntuserver.qcow,format=qcow2 -enable-kvm -cdrom ~/tmp/ubuntu-21.04-live-server-amd64.iso
```

Установка ubuntu server на диск. Создание пользователя

![](../screenshots/task1-1.jpg)

Запуск виртуальной машины без установочного образа

```
$ qemu-system-x86_64 -drive file=ubuntuserver.qcow,format=qcow2 -enable-kvm -m 4G -smp 4
```

SSH-сервер уже запущен

![](../screenshots/task1-2.png)

Запуск виртуальной машиной с работающей сетью и перенапровлением порта 22 в порт 2222 хоста

```
$ qemu-system-x86_64 -drive file=ubuntuserver.qcow,format=qcow2 -enable-kvm -m 4G -smp 4 -device virtio-net,netdev=vmnic -netdev user,id=vmnic,hostfwd=tcp:127.0.0.1:2222-:22
```

К виртуальной машине можно подключиться из хоста

![](../screenshots/task1-3.png)

Виртуальную машину можно запускать без графики (выход через `Ctrl-A X`)

```
$ qemu-system-x86_64 -drive file=ubuntuserver.qcow,format=qcow2 -enable-kvm -m 4G -smp 4 -device virtio-net,netdev=vmnic -netdev user,id=vmnic,hostfwd=tcp:127.0.0.1:2222-:22 -nographic
```

## Создание виртуальной машины с raspbian

Скачан архив с rasbian и распакован в папку с виртуальными машинами. В нём содержится img-файл с двумя разделами

![](../screenshots/task1-4.png)

Посчитаем смещение второго расдела в байтах `532480*512 = 272629760` и примонтируем его в `/mnt/raspbian`

```
# mkdir /mnt/raspbian
# mount -v -o offset=272629760 -t ext4 2020-02-13-raspbian-buster.img /mnt/raspbian
```

Закомментируем все строки в `/mnt/raspbian/etc/ld.so.preload`

Отмонтируем

```
# umount /mnt/raspbian
```

Использовано [специальное ядро и dtb-файл для qemu](https://github.com/dhruvvyas90/qemu-rpi-kernel). Я поместил их в папку для виртуальной машины.

Команда для запуска виртуальной машины 

```
$ qemu-system-arm \
  -M versatilepb \
  -cpu arm1176 \
  -m 256 \
  -drive "file=2020-02-13-raspbian-buster.img,if=none,index=0,media=disk,format=raw,id=disk0" \
  -device "virtio-blk-pci,drive=disk0,disable-modern=on,disable-legacy=off" \
  -net nic -net user,hostfwd=tcp::2223-:22 \
  -dtb versatile-pb-buster-5.4.51.dtb \
  -kernel kernel-qemu-5.4.51-buster \
  -append 'root=/dev/vda2 panic=1' \
  -no-reboot
```

В итоге у нас есть система с почти заполненным диском

![](../screenshots/task1-5.png)

Для расширения диска можно использовать cfdisk.

```
$ mv 2020-02-13-raspbian-buster.img raspbian.img # также заменить в команде запуска qemu
$ cfdisk raspbian.img
```

![](../screenshots/task1-6.jpg)

На виртуальной машине необходимо также расширить файловую систему командой `resize2fs /dev/vda2`

![](../screenshots/task1-7.png)

SSH работает так же как и на первой машине. Включить SSH, переименовать машину можно через `raspi-config`.

Туториалы:
[[1]](https://azeria-labs.com/emulate-raspberry-pi-with-qemu/)
[[2]](https://github.com/dhruvvyas90/qemu-rpi-kernel)
[[3]](https://github.com/dhruvvyas90/qemu-rpi-kernel/wiki)
