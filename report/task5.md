# Задание 5

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 11:58:01+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="95" LINES="50"]
yanush13@vm1:~$ mkdir a
yanush13@vm1:~$ mkdir a/{b,c}
yanush13@vm1:~$ mkdir -t a/c/d/e
mkdir: invalid option -- 't'
Try 'mkdir --help' for more information.
yanush13@vm1:~$ mkdir -p a/c/d/e
yanush13@vm1:~$ touch a/f
yanush13@vm1:~$ ln -s a/f a/b/f
yanush13@vm1:~$ touch a/c/d/f1
yanush13@vm1:~$ touch a/c/d/f2
yanush13@vm1:~$ touch a/c/d^C
yanush13@vm1:~$ ln a/c/d/f1 a/c/
yanush13@vm1:~$ tree a
Command 'tree' not found, but can be installed with:
sudo snap install tree  # version 1.8.0+pkg-3fd6, or
sudo apt  install tree  # version 1.8.0-1
See 'snap info tree' for additional versions.
yanush13@vm1:~$ ls -la a
total 16
drwxrwxr-x 4 yanush13 yanush13 4096 Sep  5 11:58 .
drwxr-x--- 6 yanush13 yanush13 4096 Sep  5 11:58 ..
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 11:59 b
drwxrwxr-x 3 yanush13 yanush13 4096 Sep  5 12:00 c
-rw-rw-r-- 1 yanush13 yanush13    0 Sep  5 11:58 f
yanush13@vm1:~$ ls -la b
ls: cannot access 'b': No such file or directory
yanush13@vm1:~$ ls -la a/b
total 8
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 11:59 .
drwxrwxr-x 4 yanush13 yanush13 4096 Sep  5 11:58 ..
lrwxrwxrwx 1 yanush13 yanush13    3 Sep  5 11:59 f -> a/f
yanush13@vm1:~$ ln -rs a/f a/b/f
ln: failed to create symbolic link 'a/b/f': File exists
yanush13@vm1:~$ unlink a/b/f
yanush13@vm1:~$ ln -rs a/f a/b/f
yanush13@vm1:~$ ls -la a/b
total 8
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 12:02 .
drwxrwxr-x 4 yanush13 yanush13 4096 Sep  5 11:58 ..
lrwxrwxrwx 1 yanush13 yanush13    4 Sep  5 12:02 f -> ../f
yanush13@vm1:~$ ls -la a/
b/ c/ f
yanush13@vm1:~$ ls -la a/c/
d/  f1
yanush13@vm1:~$ ls -la a/c/
d/  f1
yanush13@vm1:~$ ls -la a/c/d/
total 12
drwxrwxr-x 3 yanush13 yanush13 4096 Sep  5 11:59 .
drwxrwxr-x 3 yanush13 yanush13 4096 Sep  5 12:00 ..
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 11:58 e
-rw-rw-r-- 2 yanush13 yanush13    0 Sep  5 11:59 f1
-rw-rw-r-- 1 yanush13 yanush13    0 Sep  5 11:59 f2
yanush13@vm1:~$ ls -la a/c/d/e
total 8
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 11:58 .
drwxrwxr-x 3 yanush13 yanush13 4096 Sep  5 11:59 ..
yanush13@vm1:~$ ls -la a/b
total 8
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 12:02 .
drwxrwxr-x 4 yanush13 yanush13 4096 Sep  5 11:58 ..
lrwxrwxrwx 1 yanush13 yanush13    4 Sep  5 12:02 f -> ../f
yanush13@vm1:~$ ls -la a/
total 16
drwxrwxr-x 4 yanush13 yanush13 4096 Sep  5 11:58 .
drwxr-x--- 6 yanush13 yanush13 4096 Sep  5 11:58 ..
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 12:02 b
drwxrwxr-x 3 yanush13 yanush13 4096 Sep  5 12:00 c
-rw-rw-r-- 1 yanush13 yanush13    0 Sep  5 11:58 f
yanush13@vm1:~$ unlink a/b/f
yanush13@vm1:~$ unlink a/f ^C
yanush13@vm1:~$ ls -la a
total 16
drwxrwxr-x 4 yanush13 yanush13 4096 Sep  5 11:58 .
drwxr-x--- 6 yanush13 yanush13 4096 Sep  5 11:58 ..
drwxrwxr-x 2 yanush13 yanush13 4096 Sep  5 12:03 b
drwxrwxr-x 3 yanush13 yanush13 4096 Sep  5 12:00 c
-rw-rw-r-- 1 yanush13 yanush13    0 Sep  5 11:58 f
yanush13@vm1:~$ unlink a/f
yanush13@vm1:~$ un^C
yanush13@vm1:~$ ls
a               task3Yanush13u  task5Yanush1301u   timelog4Yanush13u    yanush
task3Yanush13r  task4Yanush13u  timelog3Yanush13u  timelog5Yanush1301u
yanush13@vm1:~$ ls ^C
yanush13@vm1:~$ unlink a/c/f1
yanush13@vm1:~$ unlink a/c/f2
unlink: cannot unlink 'a/c/f2': No such file or directory
yanush13@vm1:~$ unlink a/c/d/
e/  f1  f2
yanush13@vm1:~$ unlink a/c/d/
e/  f1  f2
yanush13@vm1:~$ unlink a/c/d/f1
yanush13@vm1:~$ unlink a/c/d/f2
yanush13@vm1:~$ rmdir a/c/d/e/
yanush13@vm1:~$ rmdir a/c/d/
yanush13@vm1:~$ rmdir a/c/
yanush13@vm1:~$ rmdir a/b
yanush13@vm1:~$ rmdir a
yanush13@vm1:~$ ssh pi@10.0.0.3
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
Linux vm2 5.4.51 #1 Sat Aug 8 23:28:32 +03 2020 armv6l

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Sun Sep  5 14:52:43 2021 from 10.0.0.1
pi@vm2:~ $ script task5Yanush1301r -T timelog5Yanush1301r
script: invalid option -- 'T'
Try 'script --help' for more information.
pi@vm2:~ $ script task5Yanush1301r
Script started, file is task5Yanush1301r
pi@vm2:~ $ tree /tmp/
/tmp/
├── ssh-HpOfIKhjXwuH
│   └── agent.375
├── ssh-IhleIyohiz4v
│   └── agent.447
└── systemd-private-827cb33ed11b490d9e9efe0067e11fe2-systemd-timesyncd.service-
74tOb7 [error opening dir]

3 directories, 2 files
pi@vm2:~ $ mkdir -p a/{b,c/d/e}
pi@vm2:~ $ tree a
a
├── b
└── c
    └── d
        └── e

4 directories, 0 files
pi@vm2:~ $ touch a/f
pi@vm2:~ $ ln -rs a/f a/b/f
pi@vm2:~ $ tree a
a
├── b
│   └── f -> ../f
├── c
│   └── d
│       └── e
└── f

4 directories, 2 files
pi@vm2:~ $ touch a/c/d/f1
pi@vm2:~ $ ln a/c/d/f1 a/c/
pi@vm2:~ $ tree a
a
├── b
│   └── f -> ../f
├── c
│   ├── d
│   │   ├── e
│   │   └── f1
│   └── f1
└── f

4 directories, 4 files
pi@vm2:~ $ touch a/c/d/f2
pi@vm2:~ $ tree a
a
├── b
│   └── f -> ../f
├── c
│   ├── d
│   │   ├── e
│   │   ├── f1
│   │   └── f2
│   └── f1
└── f

4 directories, 5 files
pi@vm2:~ $ rm -r a
pi@vm2:~ $ exit
exit
Script done, file is task5Yanush1301r
pi@vm2:~ $ exit
logout
Connection to 10.0.0.3 closed.
yanush13@vm1:~$ cd yanush/script/
yanush13@vm1:~/yanush/script$ scp pi@10.0.0.3:task5Yanush1301r
usage: scp [-346ABCpqrTv] [-c cipher] [-F ssh_config] [-i identity_file]
            [-J destination] [-l limit] [-o ssh_option] [-P port]
            [-S program] source ... target
yanush13@vm1:~/yanush/script$ scp pi@10.0.0.3:task5Yanush1301r .
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
pi@10.0.0.3's password:
task5Yanush1301r                                               0%    0     0.0K
task5Yanush1301r                                             100% 2359   330.1K
B/s   00:00
yanush13@vm1:~/yanush/script$ exit
exit

Script done on 2021-09-05 12:08:21+00:00 [COMMAND_EXIT_CODE="0"]
```

## Журнал Raspbian:

Содержится в журнале ubuntu.

```
Script started on 2021-09-05 15:04:59+03:00 [TERM="st-256color" TTY="/dev/pts/2
" COLUMNS="95" LINES="50"]
pi@vm2:~ $ tree /tmp/
/tmp/
├── ssh-HpOfIKhjXwuH
│   └── agent.375
├── ssh-IhleIyohiz4v
│   └── agent.447
└── systemd-private-827cb33ed11b490d9e9efe0067e11fe2-systemd-timesyncd.service-
74tOb7 [error opening dir]

3 directories, 2 files
pi@vm2:~ $ mkdir -p a/{b,c/d/e}
pi@vm2:~ $ tree a
a
├── b
└── c
	└── d
		└── e

4 directories, 0 files
pi@vm2:~ $ touch a/f
pi@vm2:~ $ ln -rs a/f a/b/f
pi@vm2:~ $ tree a
a
├── b
│   └── f -> ../f
├── c
│   └── d
│       └── e
└── f

4 directories, 2 files
pi@vm2:~ $ touch a/c/d/f1
pi@vm2:~ $ ln a/c/d/f1 a/c/
pi@vm2:~ $ tree a
a
├── b
│   └── f -> ../f
├── c
│   ├── d
│   │   ├── e
│   │   └── f1
│   └── f1
└── f

4 directories, 4 files
pi@vm2:~ $ touch a/c/d/f2
pi@vm2:~ $ tree a
a
├── b
│   └── f -> ../f
├── c
│   ├── d
│   │   ├── e
│   │   ├── f1
│   │   └── f2
│   └── f1
└── f

4 directories, 5 files
pi@vm2:~ $ rm -r a
pi@vm2:~ $ exit
exit

Script done on 2021-09-05 15:07:30+03:00 [COMMAND_EXIT_CODE="0"]
```
