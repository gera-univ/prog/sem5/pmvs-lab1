# Задание 4

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 09:43:40+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="95" LINES="50"]
yanush13@vm1:~$ mkdir yanush
yanush13@vm1:~$ cd yanush/
yanush13@vm1:~/yanush$ git -v
unknown option: -v
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]
yanush13@vm1:~/yanush$ git --version
git version 2.30.2
yanush13@vm1:~/yanush$ ls
yanush13@vm1:~/yanush$ git clone https://github.com/pmvs-2021/labrabota1gruppa1
3-jiffygist.git^C
yanush13@vm1:~/yanush$ git clone git@github.com:pmvs-2021/labrabota1gruppa13-ji
ffygist.git                      git@github.com:pmvs-2021/labrabota1gruppa13-ji
ffygist.git
Cloning into 'labrabota1gruppa13-jiffygist'...
The authenticity of host 'github.com (140.82.121.3)' can't be established.
RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'github.com,140.82.121.3' (RSA) to the list of known
 hosts.
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
remote: Enumerating objects: 50, done.
remote: Counting objects: 100% (50/50), done.
remote: Compressing objects: 100% (32/32), done.
remote: Total 50 (delta 11), reused 45 (delta 11), pack-reused 0
Receiving objects: 100% (50/50), 1.42 MiB | 3.27 MiB/s, done.
Resolving deltas: 100% (11/11), done.
yanush13@vm1:~/yanush$ ls
labrabota1gruppa13-jiffygist
yanush13@vm1:~/yanush$ rm -r labrabota1gruppa13-jiffygist/
rm: remove write-protected regular file 'labrabota1gruppa13-jiffygist/.git/obje
cts/pack/pack-ea70ca8ea19a6c51f325ed3a838c478e0ad22eb5.pack'? ^C
yanush13@vm1:~/yanush$ rm -r labrabota1gruppa13-jiffygist/ -f
yanush13@vm1:~/yanush$ git clone git@github.com:pmvs-2021/labrabota1gruppa13-ji
ffygist.git .
Cloning into '.'...
Enter passphrase for key '/home/yanush13/.ssh/herman_rsa':
remote: Enumerating objects: 50, done.
remote: Counting objects: 100% (50/50), done.
remote: Compressing objects: 100% (32/32), done.
remote: Total 50 (delta 11), reused 45 (delta 11), pack-reused 0
Receiving objects: 100% (50/50), 1.42 MiB | 3.38 MiB/s, done.
Resolving deltas: 100% (11/11), done.
yanush13@vm1:~/yanush$ ls
report  screenshots  tools
yanush13@vm1:~/yanush$ mkdir script
yanush13@vm1:~/yanush$ ls
report  screenshots  script  tools
yanush13@vm1:~/yanush$ cd script/
yanush13@vm1:~/yanush/script$ cp ~/t
task3Yanush13u     task4Yanush13u     timelog3Yanush13u  timelog4Yanush13u
yanush13@vm1:~/yanush/script$ cp ~/t* .
yanush13@vm1:~/yanush/script$ ls
task3Yanush13u  task4Yanush13u  timelog3Yanush13u  timelog4Yanush13u
yanush13@vm1:~/yanush/script$ rm task4Yanush13u timelog4Yanush13u # он ещё идёт
yanush13@vm1:~/yanush/script$ cp ~/
.bash_history              .profile                   task4Yanush13u
.bash_logout               .ssh/                      timelog3Yanush13u
.bashrc                    .sudo_as_admin_successful  timelog4Yanush13u
.cache/                    task3Yanush13r             .viminfo
.lesshst                   task3Yanush13u             yanush/
yanush13@vm1:~/yanush/script$ cp ~/task3Yanush13r .
yanush13@vm1:~/yanush/script$ ls
task3Yanush13r  task3Yanush13u  timelog3Yanush13u
yanush13@vm1:~/yanush/script$ exit
exit

Script done on 2021-09-05 09:52:38+00:00 [COMMAND_EXIT_CODE="0"]
```
