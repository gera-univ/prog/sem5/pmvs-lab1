# Задание 7

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 15:59:31+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="173" LINES="50"]
yanush13@vm1:~$ ps -e > ps.log
yanush13@vm1:~$ vi
vi         view       vigpg      vigr       vim        vim.basic  vimdiff    vi
m.tiny   vimtutor   vipw       visudo
yanush13@vm1:~$ vim ps.log
yanush13@vm1:~$ vim ps.log

[1]+  Stopped                 vim ps.log
yanush13@vm1:~$ fuser ^C
yanush13@vm1:~$ man fuser
yanush13@vm1:~$ fuser ps.log
yanush13@vm1:~$ fg
vim ps.log
yanush13@vm1:~$ fuser ps.log
yanush13@vm1:~$ sudo fuser ps.log
[sudo] password for yanush13:
yanush13@vm1:~$ fuser -u ps.log
yanush13@vm1:~$ fuser -u ps.log
/home/yanush13/ps.log:  1559(yanush13)
yanush13@vm1:~$ pidof top
1568
yanush13@vm1:~$ lsof >yanush-lsof.log
yanush13@vm1:~$ exit
exit

Script done on 2021-09-05 16:04:07+00:00 [COMMAND_EXIT_CODE="0"]
Script started on 2021-09-05 16:05:27+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="173" LINES="50"]
yanush13@vm1:~/yanush/script$ vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu----
-
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa s
t
 0  0      0 3541844  21892 320660    0    0    30     2   15   33  0  0 100  0
  0
yanush13@vm1:~/yanush/script$ exit
exit

Script done on 2021-09-05 16:05:34+00:00 [COMMAND_EXIT_CODE="0"]
```
