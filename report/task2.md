# Задание 2

Подключение по SSH через проброс портов (см. команды qemu в задании 1) я уже осуществлял.

```
$ ssh -p 2222 yanush13@localhost # покдлючение к виртуальной машине с ubuntu server
$ ssh -p 2223 pi@localhost # покдлючение к виртуальной машине с raspbian
```

Однако при таком подходе машины не могут видеть друг друга. Для того чтобы они были в одной сети я использовал мост.

```
# ip link add br0 type bridge
# ip addr add 10.0.0.1/24 dev br0 # даём хостовой системе ip адрес на этом устройстве 
# ip link set br0 up
```

В `/etc/qemu/bridge.conf` я раскоментировал строчку `allow br0`. 

В ubuntu server необходимо сделать сетевое устройство `ens3` опциональным в `/etc/netplan/00-installer-config.yaml`, добавив опцию `optional: true`, чтобы избежать зависания при старте.

Новая команда для включения ВМ с ubuntu server

```
$ qemu-system-x86_64 \
	-drive file=ubuntuserver.qcow,format=qcow2 \
	-enable-kvm -m 4G -smp 4 \
	-netdev bridge,id=hn1 -device virtio-net,netdev=hn1,mac=e6:c8:ff:09:76:99
```

Новая команда для включения ВМ с raspbian

```
$ qemu-system-arm \
  -M versatilepb \
  -cpu arm1176 \
  -m 256 \
  -drive "file=raspbian.img,if=none,index=0,media=disk,format=raw,id=disk0" \
  -device "virtio-blk-pci,drive=disk0,disable-modern=on,disable-legacy=off" \
  -net nic -net bridge \
  -dtb versatile-pb-buster-5.4.51.dtb \
  -kernel kernel-qemu-5.4.51-buster \
  -append 'root=/dev/vda2 panic=1' \
  -no-reboot 
```

Виртуальным машинам тоже нужно вручную добавить ip адреса на устройства и перевести их во включённое состояние

```
vm1 # ip addr add 10.0.0.2/24 dev ens3
vm1 # ip link set dev ens3 up

vm2 # ip addr del старый_ip dev eth0
vm2 # ip addr add 10.0.0.3/24 dev eth0
vm2 # ip link set dev eth0 up
```

Теперь машины видят друг друга, но у виртуальных машин нет доступа в Интернет.

![](../screenshots/task2-1.jpg)
![](../screenshots/task2-2.png)
![](../screenshots/task2-3.png)

Туториал
[[1]](https://futurewei-cloud.github.io/ARM-Datacenter/qemu/network-aarch64-qemu-guests/)

Мне удалось настроить два сетевых интерфейса на машине с ubuntu server

```
$ qemu-system-x86_64 \
	-drive file=ubuntuserver.qcow,format=qcow2 \
	-enable-kvm -m 4G -smp 4 \
	-device virtio-net,netdev=vmnic -netdev user,id=vmnic,hostfwd=tcp:127.0.0.1:2222-:22 \
	-netdev bridge,id=hn1 -device virtio-net,netdev=hn1,mac=e6:c8:ff:09:76:99
```

Можно прописать статический ip в `/etc/netplan/00-installer-config.yaml` на ubuntu server

```
# This is the network config written by 'subiquity'
network:
  ethernets:
    ens3:
      dhcp4: true
      optional: true
    ens4:
      dhcp4: false
      addresses:
        - 10.0.0.2/24
      optional: true
  version: 2
```
