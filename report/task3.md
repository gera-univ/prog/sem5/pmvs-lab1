# Задание 3

## Журнал Ubuntu Server:

```
Script started on 2021-09-05 09:33:09+00:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="95" LINES="50"]
yanush13@vm1:~$ help
GNU bash, version 5.1.4(1)-release (x86_64-pc-linux-gnu)
These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.

A star (*) next to a name means that the command is disabled.

 job_spec [&]                                   history [-c] [-d offset] [n] or
 history -an>
 (( expression ))                               if COMMANDS; then COMMANDS; [ e
lif COMMANDS>
 . filename [arguments]                         jobs [-lnprs] [jobspec ...] or
jobs -x comm>
 :                                              kill [-s sigspec | -n signum |
-sigspec] pi>
 [ arg... ]                                     let arg [arg ...]
 [[ expression ]]                               local [option] name[=value] ...
 alias [-p] [name[=value] ... ]                 logout [n]
 bg [job_spec ...]                              mapfile [-d delim] [-n count] [
-O origin] [>
 bind [-lpsvPSVX] [-m keymap] [-f filename] [>  popd [-n] [+N | -N]
 break [n]                                      printf [-v var] format [argumen
ts]
 builtin [shell-builtin [arg ...]]              pushd [-n] [+N | -N | dir]
 caller [expr]                                  pwd [-LP]
 case WORD in [PATTERN [| PATTERN]...) COMMAN>  read [-ers] [-a array] [-d deli
m] [-i text]>
 cd [-L|[-P [-e]] [-@]] [dir]                   readarray [-d delim] [-n count]
 [-O origin]>
 command [-pVv] command [arg ...]               readonly [-aAf] [name[=value] .
..] or reado>
 compgen [-abcdefgjksuv] [-o option] [-A acti>  return [n]
 complete [-abcdefgjksuv] [-pr] [-DEI] [-o op>  select NAME [in WORDS ... ;] do
 COMMANDS; d>
 compopt [-o|+o option] [-DEI] [name ...]       set [-abefhkmnptuvxBCHP] [-o op
tion-name] [>
 continue [n]                                   shift [n]
 coproc [NAME] command [redirections]           shopt [-pqsu] [-o] [optname ...
]
 declare [-aAfFgiIlnrtux] [-p] [name[=value] >  source filename [arguments]
 dirs [-clpv] [+N] [-N]                         suspend [-f]
 disown [-h] [-ar] [jobspec ... | pid ...]      test [expr]
 echo [-neE] [arg ...]                          time [-p] pipeline
 enable [-a] [-dnps] [-f filename] [name ...>   times
 eval [arg ...]                                 trap [-lp] [[arg] signal_spec .
..]
 exec [-cl] [-a name] [command [argument ...]>  true
 exit [n]                                       type [-afptP] name [name ...]
 export [-fn] [name[=value] ...] or export ->   typeset [-aAfFgiIlnrtux] [-p] n
ame[=value] >
 false                                          ulimit [-SHabcdefiklmnpqrstuvxP
T] [limit]
 fc [-e ename] [-lnr] [first] [last] or fc -s>  umask [-p] [-S] [mode]
 fg [job_spec]                                  unalias [-a] name [name ...]
 for NAME [in WORDS ... ] ; do COMMANDS; don>   unset [-f] [-v] [-n] [name ...]
 for (( exp1; exp2; exp3 )); do COMMANDS; don>  until COMMANDS; do COMMANDS; do
ne
 function name { COMMANDS ; } or name () { CO>  variables - Names and meanings
of some shel>
 getopts optstring name [arg ...]               wait [-fn] [-p var] [id ...]
 hash [-lr] [-p pathname] [-dt] [name ...]      while COMMANDS; do COMMANDS; do
ne
 help [-dms] [pattern ...]                      { COMMANDS ; }
yanush13@vm1:~$ ls
task3Yanush13u  timelog3Yanush13u
yanush13@vm1:~$ cd /tmp/
yanush13@vm1:/tmp$ pwd
/tmp
yanush13@vm1:/tmp$ mkdir dir
yanush13@vm1:/tmp$ rmdir dir
yanush13@vm1:/tmp$ mkdir dir2
yanush13@vm1:/tmp$ rm -r dir2
yanush13@vm1:/tmp$ hisotry 5
hisotry: command not found
yanush13@vm1:/tmp$ history 5
   69  rmdir dir
   70  mkdir dir2
   71  rm -r dir2
   72  hisotry 5
   73  history 5
yanush13@vm1:/tmp$ cd
yanush13@vm1:~$ exit
exit

Script done on 2021-09-05 09:34:11+00:00 [COMMAND_EXIT_CODE="0"]
```

## Журнал Raspbian:

```
Script started on 2021-09-05 12:41:48+03:00 [TERM="st-256color" TTY="/dev/pts/0
" COLUMNS="77" LINES="50"]
pi@vm2:~ $ mkdir yanush
pi@vm2:~ $ cal
   September 2021
Su Mo Tu We Th Fr Sa
          1  2  3  4
 5  6  7  8  9 10 11
12 13 14 15 16 17 18
19 20 21 22 23 24 25
26 27 28 29 30

pi@vm2:~ $ date
Sun 05 Sep 2021 12:42:00 PM +03
pi@vm2:~ $ who
pi       tty1         2021-09-05 12:17
pi       tty7         2021-09-05 12:17 (:0)
pi       pts/0        2021-09-05 12:17 (10.0.2.2)
pi@vm2:~ $ whoami
pi
pi@vm2:~ $ cd yanush/
pi@vm2:~/yanush $ echo "the ccow jumped ovver the mooon" | tee cow
the ccow jumped ovver the mooon
pi@vm2:~/yanush $ cat cow
the ccow jumped ovver the mooon
pi@vm2:~/yanush $ vi cow
pi@vm2:~/yanush $ cat cow
the cow jumped over the moon
pi@vm2:~/yanush $ cp cow cow2
pi@vm2:~/yanush $ unlink cow2
pi@vm2:~/yanush $ man cp
pi@vm2:~/yanush $ man unlink
pi@vm2:~/yanush $ exit
exit

Script done on 2021-09-05 12:43:10+03:00 [COMMAND_EXIT_CODE="0"]

```
