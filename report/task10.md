# Задание 10

Качаем образ nginx

```
# docker pull nginx
```

Создаём контейнер

```
# docker run -d -p 2222:22 --name my-nginx-server nginx
```

Подключаемся к контейнеру через docker

```
# docker exec -it my-nginx-server /bin/bash
```

Устанавливаем SSH сервер

```
# apt install openssh-server
```

Запускаем SSH сервер

```
# /etc/init.d/ssh start
```

Создаём пользователя

```
# adduser herman
```

Узнаём ip контейнера

```
# docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' my-nginx-server
```

Подключаемся

```
$ ssh herman@172.17.0.2
```
